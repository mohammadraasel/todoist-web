import React, { useState } from 'react'
import { FaTrashAlt} from 'react-icons/fa'
import { useProjectsValue, useSelectedProjectValue } from '../../context'
import {db} from '../../firebase'

const ProjectItem = ({ project }) => {
    const [showConfirm, setShowConfirm] = useState(false)
    const { projects, setProjects } = useProjectsValue()
    const { setSelectedProject } = useSelectedProjectValue()
    
    const deleteProject = docId => {
        db.collection('projects')
            .doc(docId)
            .delete()
            .then(() => {
                setProjects([...projects])
                setSelectedProject('INBOX')
            })
    }
    return (
        <>
            <span className='sidebar__project-dot'></span>
            <span className='sidebar__project-title'>{project.title}</span>
            <span
                className='sidebar__project-delete'
                data-testid='delete-project'
                onClick={() => setShowConfirm(!showConfirm)}
                onKeyDown={() => setShowConfirm(!showConfirm)}
            >
                <FaTrashAlt />
                {
                    showConfirm && (
                        <div className="project-delete-modal">
                            <div className="project-delete-modal__inner">
                                <p>Are you sure want to delete this project?</p>
                                <div className="action-buttons">
                                    <button className='action-buttons__delete' onClick={()=> deleteProject(project.id)}>
                                        Delete
                                    </button>
                                    <button className='action-buttons__cancel' onClick={()=> setShowConfirm(!showConfirm)}>
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    )
                }
                
            </span>
        </>
    )
}

export default ProjectItem
