import React from 'react'
import {useProjectsValue} from '../../context'

const ProjectOverlay = ({ setProject, showProjectOverlay, setShowProjectOverlay }) => {
    
    const { projects } = useProjectsValue()
    return (

        projects && showProjectOverlay &&(
            <div className="project-overlay" data-testid="project-overlay">
                <ul className="project-overlay__list">
                    {
                        projects.map(project => (
                            <li
                                data-testid="project-overlay-action"
                                key={project.id}
                                onClick={() => {
                                    setProject(project.id)
                                    setShowProjectOverlay(false)

                                }}
                            >
                                {project.title}
                            </li>
                        ))
                    }
                </ul>
            </div>
        )
    )
}

export default ProjectOverlay
