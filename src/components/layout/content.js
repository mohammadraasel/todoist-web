import React from 'react'
import Sidebar from './sidebar'
import Tasks from '../tasks'

const Content = () => {
    return (
        <section className='content'>
            <Sidebar />
            <Tasks/>
        </section>
    )
}

export default Content
