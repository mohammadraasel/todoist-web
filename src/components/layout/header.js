import React, {useState} from 'react'
import { FaPizzaSlice, FaPlus } from 'react-icons/fa'
import { logo } from '../../constants/images'
import AddTask from '../add-task'

const Header = ({ darkMode, setDarkMode }) => {
    const [shouldShowMain, setshouldShowMain] = useState(false)
    const [showQuickAddTask, setShowQuickAddTask] = useState(false)
    return (
        <header className="header" data-testid="header">
            <nav className='nav'>
                <div className="logo">
                    <img src={logo} alt="todoist logo"/>
                </div>
                <div className="settings">
                    <ul>
                        <li
                            onClick={() => {
                                setShowQuickAddTask(true)
                                setshouldShowMain(true)
                            }}
                            data-testid='quick-add-task-action'
                            className="settings__add">
                            <FaPlus />
                        </li>
                        <li
                            onClick={()=> setDarkMode(!darkMode)}
                            data-testid='dark-mode-action'
                            className='settings__darkmode'
                        >
                            <FaPizzaSlice />
                        </li>
                    </ul>
                </div>
            </nav>
            <AddTask
                showAddTaskMain={false}
                showQuickAddTask={showQuickAddTask}
                setShowQuickAddTask={setShowQuickAddTask}
                shouldShowMain={shouldShowMain}
            />
        </header>
    )
}

export default Header
