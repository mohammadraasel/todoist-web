import React, { useState } from 'react'
import { FaChevronDown, FaInbox, FaRegCalendarAlt, FaRegCalendar} from 'react-icons/fa'
import { useSelectedProjectValue } from '../../context'
import Projects from '../projects'
import AddProject from '../add-project'

const Sidebar = () => {

    const { setSelectedProject } = useSelectedProjectValue()
    const [active, setActive] = useState('inbox')
    const [showProjects, setShowPojects] = useState(false)


    return (
        <div className="sidebar" data-testid="sidebar">
            <ul className="sidebar__generic">
                <li className={`inbox ${active=== 'inbox'? 'active': ''}`} data-testid='inbox'
                    onClick={() => {
                        setActive('inbox')
                        setSelectedProject('INBOX')
                    }}
                >
                    <span><FaInbox/></span>
                    <span>Inbox</span>
                </li>
                <li className={`today ${active=== 'today'? 'active': ''}`} data-testid='today'
                    onClick={() => {
                        setActive('today')
                        setSelectedProject('TODAY')
                    }}
                >
                    <span><FaRegCalendar/></span>
                    <span>Today</span>
                </li>
                <li className={`next-7-days ${active=== 'next-7-days'? 'active': ''}`} data-testid='next-7-days'
                    onClick={() => {
                        setActive('next-7-days')
                        setSelectedProject('NEXT_7_DAYS')
                    }}
                >
                    <span><FaRegCalendarAlt/></span>
                    <span>Next 7 days</span>
                </li>               
            </ul>

            <div className="sidebar__middle" onClick={() => {
                setShowPojects(!showProjects)
            }}>
                <span><FaChevronDown className={!showProjects? 'hidden-projects': ''} /></span>
                <h2>Projects</h2>
            </div>

            <ul className="sidebar__projects">
                {
                    showProjects && <Projects/>
                }
            </ul>

            {
                showProjects && <AddProject/>
            }
            
        </div>
    )
}

export default Sidebar
