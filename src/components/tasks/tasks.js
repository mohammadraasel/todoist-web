import React, {useEffect} from 'react'
import Checkbox from '../checkbox'
import { useTasks } from '../../hooks'
import { collatedTasks } from '../../constants'
import { getTitle, getCollatedTitle, collatedTasksExist } from '../../helpers'

import { useSelectedProjectValue, useProjectsValue } from '../../context'

import AddTask from '../add-task'

 
// const selectedProjectId = '06Rz8cjY38m0o0QNocKx'


const Tasks = () => {
    const { selectedProject } = useSelectedProjectValue()
    const { projects } = useProjectsValue()
    const { tasks } = useTasks(selectedProject)
    
    let projectName = ''
    if (projects && selectedProject && !collatedTasksExist(selectedProject)) {
        projectName = getTitle(projects, selectedProject)
    }

    if (collatedTasksExist(selectedProject) && selectedProject) {
        projectName = getCollatedTitle(collatedTasks, selectedProject)
    }

    useEffect(() => {
        document.title = `${projectName}: Todoist`
    })

    return (
        <div className="tasks" data-testid="tasks">
            <h2 data-testid="project-name">{projectName}</h2>
            <ul className="tasks__list">
                {
                    tasks.length !== 0? 
                        tasks.map(task => (
                            <li key={task.id}>
                                <Checkbox id={task.id} />
                                <span>{task.title}</span>
                            </li>
                        ))
                    : <li>Loading...</li>
                }
            </ul>

            <AddTask/>
        </div>
    )
}

export default Tasks
