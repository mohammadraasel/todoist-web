import React from 'react'
import moment from 'moment'
import { FaSpaceShuttle, FaSun, FaRegPaperPlane } from 'react-icons/fa'

const TaskDate = ({ setTaskDate, showTaskDate, setShowTaskDate }) => {
    return (
        showTaskDate && (
            <div
                className="task-date"
                data-testid="task-date-overlay"
            >
                <ul className="task-date__list">
                    <li
                        onClick={() => {
                            setShowTaskDate(false)
                            setTaskDate(new Date(moment().format('MM/DD/YYYY')))
                        }}
                        data-testid="task-date-today"
                    >
                        <span>
                            <FaSpaceShuttle/>
                        </span>
                        <span>Today</span>
                    </li>
                    <li
                        onClick={() => {
                            setShowTaskDate(false)
                            setTaskDate(new Date(moment().add(1, 'days').format('MM/DD/YYYY')))
                        }}
                        data-testid="task-date-tomorrow"
                    >
                        <span>
                            <FaSun/>
                        </span>
                        <span>Tomorrow</span>
                    </li>
                    <li
                        onClick={() => {
                            setShowTaskDate(false)
                            setTaskDate(new Date(moment().add(7, 'days').format('MM/DD/YYYY')))
                        }}
                        data-testid="task-date-next-week"
                    >
                        <span>
                            <FaRegPaperPlane/>
                        </span>
                        <span>Next week</span>
                    </li>
                </ul>
            </div>
        )
    )
}

export default TaskDate
