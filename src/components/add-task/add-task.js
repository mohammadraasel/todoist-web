import React, { useState } from 'react'
import { FaRegListAlt, FaRegCalendarAlt } from 'react-icons/fa'
import moment from 'moment'
import { db } from '../../firebase'
import { useSelectedProjectValue } from '../../context/selected-project-context'
import { currentUserId } from '../../constants'
import { generatePushId } from '../../helpers'
import ProjectOverlay from '../project-overlay/project-overlay'
import TaskDate from '../task-date/task-date'



const AddProject = ({ showAddTaskMain = true, shouldShowMain = false, showQuickAddTask, setShowQuickAddTask }) => {
    
    const [task, setTask] = useState('')
    const [taskDate, setTaskDate] = useState('')
    const [project, setProject] = useState('')
    const [showMain, setShowMain] = useState(shouldShowMain)
    const [showProjectOverlay, setShowProjectOverlay] = useState(false)
    const [showTaskDate, setShowTaskDate] = useState(false)
    const taskId = generatePushId()
    const { selectedProject } = useSelectedProjectValue()

    const addTask = () => {
        const projectId = project || selectedProject;

        let collatedDate = ''

        if (projectId === "TODAY") {
            collatedDate = new Date(moment().format('MM/DD/YYYY'))
        } else if (projectId === 'NEXT_7_DAYS') {
           collatedDate = new Date(moment().add(7, 'days').format('MM/DD/YYYY')) 
        }

        return task && projectId && db.collection('tasks').add({
            id: taskId,
            archived: false,
            projectId: projectId,
            title: task,
            userId: currentUserId,
            date: collatedDate || taskDate || new Date()
        }).then(() => {
            setTask('')
            setProject('')
            setShowMain(false)
            setShowProjectOverlay(false)
            
        })
    }
    return (
        <div
            className={showQuickAddTask ? "add-task add-task__overlay" : "add-task"}
            data-testid="add-task-comp"
        >
            {
                showAddTaskMain && 
                <div
                    className="add-task__shallow"
                    data-testid="show-main-action"
                    onClick={() => setShowMain(!showMain)}
                >
                    <span className="add-task__text">
                        <span className="add-task__plus">+</span>
                        Add Task
                    </span>
                    
                </div>
            }

            {
                (showMain || showQuickAddTask) && (
                    <div className="add-task__main" data-testid="add-task-main">
                        {
                            showQuickAddTask && (
                                <>
                                    <div data-testid='quick-add-task'>
                                        <h2 className='add-task-header'>Quick Add Task</h2>
                                        <span
                                            role='img'
                                            aria-label='cross-moji'
                                            className='add-task__cancel-x'
                                            data-testid="add-task-quick-cancel"
                                            onClick={() => {
                                                setShowMain(false)
                                                setShowProjectOverlay(false)
                                                setShowQuickAddTask(false)
                                            }}
                                        >×</span>
                                    </div>
                                </>
                            )
                        }

                        <ProjectOverlay
                            setProject={setProject}
                            showProjectOverlay={showProjectOverlay}
                            setShowProjectOverlay = {setShowProjectOverlay}
                        />
                        <TaskDate
                            setTaskDate={setTaskDate}
                            showTaskDate={showTaskDate}
                            setShowTaskDate={setShowTaskDate}
                        />
                        <input type="text"
                            className="add-task__content"
                            data-testid='add-task-content'
                            placeholder="Type task title here..."
                            value={task}
                            onChange={(e) =>setTask(e.target.value)} 
                        />

                        <button
                            type="button"
                            className="add-task__submit"
                            data-testid="add-task"
                            onClick={() => {
                                addTask()
                                if (showQuickAddTask) {
                                    setShowQuickAddTask(false)
                                }
                            }}
                        >
                            Add Task
                        </button>

                        {
                            !showQuickAddTask && (
                                <button
                                    className="add-task__cancel"
                                    data-testid="add-task-main-cancel"
                                    onClick={() => {
                                        setShowMain(false)
                                        setShowProjectOverlay(false)
                                    }}
                                >
                                    Cancel
                                </button>
                            )
                        }

                        <span
                            className="add-task__project"
                            data-testid="show-project-overlay"
                            onClick={()=> setShowProjectOverlay(!showProjectOverlay)}
                        >
                            <FaRegListAlt/>
                        </span>
                        <span
                            className="add-task__date"
                            data-testid="show-task-date-overlay"
                            onClick={()=> setShowTaskDate(!showTaskDate)}
                        >
                            <FaRegCalendarAlt/>
                        </span>

                    </div>
                )
            }
        </div>
    )
}

export default AddProject
