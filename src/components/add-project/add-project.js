import React, { useState } from 'react'
import { db } from '../../firebase'
import { useProjectsValue } from '../../context'
import { currentUserId } from '../../constants'
import { generatePushId } from '../../helpers'

const AddProject = ({ shouldShow = false }) => {
    const [show, setShow] = useState(shouldShow)
    const [projectName, setProjectName] = useState('');

    const projectId = generatePushId()
    const {setProjects} = useProjectsValue()

    const addProject = () => {
        projectName && db.collection('projects').add({
            id: projectId,
            title: projectName,
            userId: currentUserId,
            createdAt: new Date()
        }).then(() => {
            setProjects([])
            setProjectName('')
            setShow(false)
        })
    }

    return (
        <div className="add-project" data-testid='add-project'>
            {
                show && <div className="add-project__input">
                    <input
                        value={projectName}
                        onChange={(e) => setProjectName(e.target.value)}
                        className="add-project__name"
                        data-testid="project-name"
                        type="text"
                        placeholder="Name your project"
                    />

                    <button
                        className="add-project__submit"
                        type="button"
                        data-testid="add-project-submit"
                        onClick={() => addProject()}
                    >
                        Add Project
                    </button>
                    <button
                        className="add-project__cancel"
                        data-testid="hide-project-overlay"
                        onClick={()=> setShow(false)}
                    >
                        Cancel
                    </button>
                    
                </div>
            }

                    <span
                        data-testid="add-project-action"
                        className="add-project__text"
                        onClick={()=>setShow(!show)}
                    >
                        <span className="add-project__plus">+</span>
                        Add Project
                    </span>
            
        </div>
    )
}

export default AddProject
