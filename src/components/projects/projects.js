import React, { useState } from 'react'
import { useSelectedProjectValue, useProjectsValue } from '../../context'
import ProjectItem from '../project-item'

const Projects = ({ activeId = null }) => {
    const [activeProjectId, setActiveProjectId] = useState(activeId)
    const { setSelectedProject } = useSelectedProjectValue()
    const { projects } = useProjectsValue()
    
    return (
        projects && projects.map(project => (
            <li
                key={project.id}
                data-doc-id={project.id}
                data-testid='project-action'
                className={
                    activeProjectId === project.id ? 'active sidebar__project' : 'sidebar__project'
                }
                onKeyDown={()=>{
                    setActiveProjectId(project.id)
                    setSelectedProject(project.id)
                }}
                onClick={() => {
                    setActiveProjectId(project.id)
                    setSelectedProject(project.id)
                }}
            >
                <ProjectItem project={project}/>
            </li>
        ))
    )
}

export default Projects
