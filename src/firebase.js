import app from 'firebase/app'
import 'firebase/firestore'

const config = {
  apiKey: "AIzaSyDW0jjtUdB2dSjdzqmxGsZIv-PcOlxPVd0",
  authDomain: "todoist-6d0a8.firebaseapp.com",
  databaseURL: "https://todoist-6d0a8.firebaseio.com",
  projectId: "todoist-6d0a8",
  storageBucket: "todoist-6d0a8.appspot.com",
  messagingSenderId: "214812400360",
  appId: "1:214812400360:web:37b1a944a0f99c471e875d"
}

const firebase = app.initializeApp(config)
const db = firebase.firestore()

export { firebase, db }