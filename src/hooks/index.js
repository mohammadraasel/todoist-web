import { useEffect, useState } from 'react'
import { db } from '../firebase'
import moment from 'moment'
import { collatedTasksExist } from '../helpers'
import { currentUserId } from '../constants'


const getDateWithTime = (date, hours, minutes, seconds) => {
    return new Date(date.getFullYear(), date.getMonth(), date.getDate(), hours, minutes, seconds);
 }

export const useTasks = selectedProject => {

    const [tasks, setTasks] = useState([])
    const [archivedTasks, setArchivedTasks] = useState([])
  
    useEffect(() => {
        let query = db.collection('tasks').where('userId', '==', currentUserId)

        if (selectedProject && !collatedTasksExist(selectedProject)) {
            query = query.where('projectId', '==', selectedProject)
        } else if (selectedProject === 'TODAY') {
            query = query.where('date', '>=', getDateWithTime(new Date(), 0, 0, 0)).where('date', '<=', getDateWithTime(new Date(), 23, 59, 59))
        } else if (selectedProject === 'INBOX' || selectedProject === 0) {
            query = query.orderBy('date')
        }

        let unsubscribe = query.onSnapshot(snapshot => {
            const newTasks = snapshot.docs.map(task => {
                return {
                    ...task.data(),
                    id: task.id
                }
            })
           
            const filteredTasks = selectedProject === 'NEXT_7_DAYS' ?
                newTasks.filter(task => moment(task.date.toDate(), 'DD MM YYYY').diff(moment(), 'days') <= 7 && task.archived !== true )
                :
                newTasks.filter(task => task.archived !== true)
            
            setTasks(filteredTasks)

            setArchivedTasks(newTasks.filter(task => task.archived === true))
        })

        return () => {
            unsubscribe()
        }
    }, [selectedProject])


    return { tasks , archivedTasks }
}



export const useProjects = () => {
    const [projects, setProjects] = useState([])

    useEffect(() => {
        db.collection('projects')
            .where('userId', '==', currentUserId)
            .orderBy('id')
            .get()
            .then(snapshot => {
                const allProjects = snapshot.docs.map(project => {
                    return {
                        id: project.id,
                       ...project.data()
                    }
                })


                if (JSON.stringify(allProjects) !== JSON.stringify(projects)) {
                    setProjects(allProjects)
                }
            })

    }, [projects])

    return {projects, setProjects}
}