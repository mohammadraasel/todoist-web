import React, { useState } from 'react';
import './app.scss'
import {Header, Content} from '../components/layout'
import { ProjectsProvider, SelectedProjectProvider } from '../context'

function App({ darkModeDefault = false }) {
  
  const [darkMode , setDartMode] = useState(darkModeDefault)
  return (
    <SelectedProjectProvider>
      <ProjectsProvider>
        <main
          className={darkMode? 'darkmode': ''}
          data-testid="application"
        >
          <Header darkMode={darkMode} setDarkMode={setDartMode}/>
          <Content/>
        </main>
      </ProjectsProvider>
    </SelectedProjectProvider>
    
  );
}

export default App
